//
//  CustomTableViewCell.swift
//  TelegramFaceOff
//
//  Created by Gleb on 04.12.2019.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var nameOfContact: UILabel!
    @IBOutlet weak var imageOfContact: UIImageView!
    @IBOutlet weak var recentMesssage: UILabel!
    @IBOutlet weak var timeOfMessage: UILabel!
    @IBOutlet weak var unreadMessageNumber: UILabel!
    @IBOutlet weak var checkedMessageImage: UIImageView!
    
    @IBOutlet weak var mutedContact: UIImageView!
    
    @IBOutlet weak var pinnedImage: UIImageView!
    //    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
