//
//  DetailsViewController.swift
//  TelegramFaceOff
//
//  Created by Gleb on 08.12.2019.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    var name = ""
    var recentMessage = ""
    @IBOutlet weak var titleDelegated: UINavigationItem!
    @IBOutlet weak var inWindowLabel: UILabel!
    
    @IBOutlet weak var recentMessagesDelegated: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleDelegated.title = name
        inWindowLabel.text = name
        recentMessagesDelegated.text = recentMessage
        
        print("view did load")
        // Do any additional setup after loading the view.
    }
    
    func addMenuItems(menu:String ...) -> [UIPreviewActionItem] {
        var arrPreview = [UIPreviewActionItem]()
        for m in menu {
            let likeAction = UIPreviewAction(title:m, style: .default) { (action, viewController) -> Void in
                print(action.title)
            }
            arrPreview.append(likeAction)
        }
        return arrPreview
    }
    
    // Add Action of preview
    override var previewActionItems: [UIPreviewActionItem] {
        return self.addMenuItems(menu: "Open","Bookmark")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
