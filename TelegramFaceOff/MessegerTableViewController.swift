//
//  MessegerTableViewController.swift
//  TelegramFaceOff
//
//  Created by Gleb on 03.12.2019.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit


class MessegerTableViewController: UITableViewController {
    var contactInfo = ["Andrey", "Dima", "Elena", "Rita", "John", "Mauro", "Nastya", "Olga", "Karina"]
    var messageInfo = ["12:40", "13:13", "16:01", "18:18", "Thu", "Mon", "28/11", "16/11", "11/11", "5/11"]
    var readMessageInfo = ["3", "1", "0", "1", "2", "12", "6", "8", "0"]
    var statusRead = ["checkmark", "doubleCheckmark", "", "checkmark", "doubleCheckmark", "checkmark", "doubleCheckmark", "", "checkmark"]
    var recentMessages = ["Confirmed names until now for next Cisco exams", "yes", "Before taking the exam you will be asked to sign", "Keep us updated with your match", "Hope is not raining", "Hey guys! What do you think about playing some tennis this saturday afternoon? Are you in?", "coming", "we are about to start", "Ciao"]
    var mutedStatus = ["unmuted", "muted", "unmuted", "unmuted", "unmuted", "muted", "unmuted", "muted", "unmuted"]
    var transferArray = ["","","","","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()

        
        //        mainSearchBar.tintColor = .systemBlue
//        mainSearchBar.backgroundColor = .systemBackground
//        overrideUserInterfaceStyle = .dark
        //        searchController.searchResultsUpdater = self
        //        searchController.obscuresBackgroundDuringPresentation = false
        //        searchController.searchBar.placeholder = "Search"
        //        navigationItem.searchController = searchController
        //        definesPresentationContext = true
        //search controller setup
    }
    // MARK: - Table view data source
    
    @IBOutlet weak var mainSearchBar: UISearchBar!
    
    @IBAction func editButton(_ sender: Any) {
        if tableView.allowsMultipleSelectionDuringEditing == false {
            
            tableView.allowsMultipleSelectionDuringEditing = true
//          tableView.accessibilityRespondsToUserInteraction = true
            tableView.allowsMultipleSelection = true
            tableView.setEditing(true, animated: true)
            self.navigationItem.leftBarButtonItem = self.editButtonItem
            navigationItem.leftBarButtonItem?.title = "Done"
        }
        else {
//                   tableView.setEditing(false, animated: true)
//                   self.navigationItem.leftBarButtonItem!.title = "Edit"
//                   tableView.allowsMultipleSelectionDuringEditing = false
//                   tableView.allowsMultipleSelection = false
//                   tableView.accessibilityRespondsToUserInteraction = false
            // не ебу почему не работает елс и при слайд меню "едит" меняется на "дан"
            
        }
       
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return contactInfo.count
    }
    
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let readAction = UIContextualAction(style: .normal, title:  "Read", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            if self.readMessageInfo[indexPath.row] != "0"{
                self.readMessageInfo[indexPath.row] = "0"
            } else {
            }
            print("read Action...") //add action & animation
            success(true)
            tableView.reloadData()
        })
        readAction.backgroundColor = .gray
        let pinAction = UIContextualAction(style: .normal, title:  "Pin", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.transferArray[0] = self.contactInfo[indexPath.row]
            self.transferArray[1] = self.messageInfo[indexPath.row]
            self.transferArray[2] = self.readMessageInfo[indexPath.row]
            self.transferArray[3] = self.statusRead[indexPath.row]
            self.transferArray[4] = self.recentMessages[indexPath.row]
            self.transferArray[5] = self.mutedStatus[indexPath.row]
 
            self.contactInfo.remove(at: indexPath.row)
            self.messageInfo.remove(at: indexPath.row)
            self.readMessageInfo.remove(at: indexPath.row)
            self.statusRead.remove(at: indexPath.row)
            self.recentMessages.remove(at: indexPath.row)
            self.mutedStatus.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)

            self.contactInfo.insert(self.transferArray[0], at: 0)
            self.messageInfo.insert(self.transferArray[1], at: 0)
            self.readMessageInfo.insert(self.transferArray[2], at: 0)
            self.statusRead.insert(self.transferArray[3], at: 0)
            self.recentMessages.insert(self.transferArray[4], at: 0)
            self.mutedStatus.insert(self.transferArray[5], at: 0)
            tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)

            print("pin Action...")
            success(true)
        })
        pinAction.backgroundColor = .green
        return UISwipeActionsConfiguration(actions: [readAction, pinAction])
    }
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        // Write action code for the mute button
        let unmuteAction = UIContextualAction(style: .normal, title:  "Mute", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            if self.mutedStatus[indexPath.row] == "muted"{
                self.mutedStatus[indexPath.row] = "unmuted"
            }
            else {
                self.mutedStatus[indexPath.row] = "muted"
            }
            print("Mute action ...")
            
            success(true)
            tableView.reloadData()
            
        })
        unmuteAction.backgroundColor = .orange
        
        let deleteAction = UIContextualAction(style: .destructive, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.contactInfo.remove(at: indexPath.row)
            self.messageInfo.remove(at: indexPath.row)
            self.readMessageInfo.remove(at: indexPath.row)
            self.statusRead.remove(at: indexPath.row)
            self.recentMessages.remove(at: indexPath.row)
            self.mutedStatus.remove(at: indexPath.row)
            tableView.deleteRows(at:[indexPath], with: .automatic)
            print("delete action ...")//add action & animation
            success(true)
        })
        deleteAction.backgroundColor = .red
        
        // Write action code for the More
        let archiveAction = UIContextualAction(style: .normal, title:  "Archive", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
           self.contactInfo.remove(at: indexPath.row)
           self.messageInfo.remove(at: indexPath.row)
           self.readMessageInfo.remove(at: indexPath.row)
           self.statusRead.remove(at: indexPath.row)
           self.recentMessages.remove(at: indexPath.row)
           self.mutedStatus.remove(at: indexPath.row)
           tableView.deleteRows(at:[indexPath], with: .left)
            print("Archived") //add action & animation
            success(true)
//            tableView.reloadData()
        })
        archiveAction.backgroundColor = .gray
        return UISwipeActionsConfiguration(actions: [deleteAction,archiveAction,unmuteAction])
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        cell.imageOfContact.image = UIImage(named: contactInfo[indexPath.row])
        cell.nameOfContact.text = contactInfo[indexPath.row]
        cell.recentMesssage.text = recentMessages[indexPath.row]
        cell.imageOfContact.layer.cornerRadius = cell.imageOfContact.frame.size.height / 2
        
        cell.imageOfContact.clipsToBounds = true
        cell.timeOfMessage.text = messageInfo[indexPath.row]
        cell.unreadMessageNumber.text = "  " + readMessageInfo[indexPath.row] + "  "
        cell.mutedContact.image = UIImage(named: mutedStatus[indexPath.row])
        if mutedStatus[indexPath.row] != "muted" {
            cell.mutedContact.isHidden = true
            cell.unreadMessageNumber.backgroundColor = .systemBlue
        }
        else{
            cell.mutedContact.isHidden = false
            cell.unreadMessageNumber.backgroundColor = .systemGray3
        }
        
        if cell.unreadMessageNumber.text == "  0  " {
            cell.unreadMessageNumber.isHidden = true
        }
        else {}
        
        
        cell.unreadMessageNumber.layer.cornerRadius = cell.unreadMessageNumber.frame.size.height / 2
        cell.unreadMessageNumber.clipsToBounds = true
        cell.unreadMessageNumber.font = UIFont.systemFont(ofSize: 11.0)
        cell.checkedMessageImage.image = UIImage(named: statusRead[indexPath.row])
        
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing != true {
            readMessageInfo[indexPath.row] = "0"
            tableView.reloadData()
        }
        else{
            view.endEditing(true)
        }
    }
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return !tableView.isEditing
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "segueDetails" else { return }
        guard let destination = segue.destination as? DetailsViewController else { return }
        let cell = tableView.indexPathForSelectedRow?.row
        if cell == nil{}
        else {
            destination.name = contactInfo[cell!]
            destination.recentMessage = recentMessages[cell!]
        }
        print("prefer for segue")
    }
    
  @IBAction func cancelAction(_ segue:UIStoryboardSegue) {}
}

extension MessegerTableViewController: UISearchBarDelegate {
    //скрываем клавиатуру по нажатию снаружи
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
